import os, sys

proj_path = "/Users/huanhuanw/autoform/"
# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "autoform.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# This is the start of custom script
import pdf
from web.models import TagMap

# python preprocess.py <filename> <template_id>
filename = sys.argv[1]
template_id = sys.argv[2]

filename = os.path.join(proj_path, 'visa', 'processed', filename)
tags = pdf.getFormFieldList(filename)

for tag in tags:
	item, created = TagMap.objects.get_or_create(tag=tag, template_id=template_id)
	item.save()