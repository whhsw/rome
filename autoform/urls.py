"""autoform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

import web.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^login/', TemplateView.as_view(template_name='login.html')),

    url(r'^$', web.views.index),
    url(r'^applications$', web.views.applications, name='application_list'),
    url(r'^application/choose/$', web.views.application_choose_template),
    url(r'^application/create/$', web.views.application_create, name='application_create'),
    url(r'^application/(?P<application_id>[0-9]+)/$', web.views.application_detail, name='application_detail'),
    url(r'^share/create/(?P<application_id>[0-9]+)', web.views.application_share_create, name='application_share_create'),
    url(r'^share/view/(?P<token>[0-9a-zA-Z]+)', web.views.application_share_view, name='application_share_view'),
    url(r'^share/copy/(?P<token>[0-9a-zA-Z]+)', web.views.application_share_copy, name='application_share_copy'),

    url(r'^upload$', web.views.upload, name='upload'),

    url(r'^profiles$', web.views.profiles),
    url(r'^profile/(?P<profile_id>[0-9]+)/$', web.views.profile_detail, name='profile_detail'),
    url(r'^profile/save/$', web.views.profile_save_data, name='profile_save_data'),
    url(r'^profile/create/$', web.views.profile_create, name='profile_create'),

    url(r'^documents$', web.views.documents),
    url(r'^autosave$', web.views.autosave),
    url(r'^preview/(?P<application_id>[0-9]+)/$', web.views.preview, name='preview_form'),
    url(r'^preview_process/(?P<application_id>[0-9]+)/$', web.views.preview_process, name='preview_process'),

    url(r'^templates$', web.views.templates),
    url(r'^template/(?P<template_id>[0-9]+)$', web.views.template_detail, name='template_detail'),
    url(r'^template/delete/(?P<template_id>[0-9]+)$', web.views.template_delete, name='template_delete'),

    url(r'^collect_tags$', web.views.collect_tags, name='collect_tags'),
]
