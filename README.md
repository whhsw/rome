# AutoForm

# Setup environment

1. Install virtual env
https://virtualenvwrapper.readthedocs.io/en/latest/

```
sudo pip install virtualenv virtualenvwrapper
echo "export WORKON_HOME=~/Env" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
source ~/.bashrc
mkvirtualenv hackthon
```

Install ImageMagik
export MAGICK_HOME=~/homebrew

Install PDFTK in mac
Reference: https://gist.github.com/jvenator/9672772a631c117da151
```
install_pdftk.sh https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/pdftk_server-2.02-mac_osx-10.11-setup.pkg
```

2. Install mysql in your mac

3. Acitvate env and install required package
workon hackthon;
pip install -r setup/requirements.txt;
brew install ghostscript;

4. Remove text field border
Delete line 410 of ~/Env/hackthon/lib/python2.7/site-packages/reportlab/pdfbase/pdfform.py
'/MK << /BC [ 0 0 0 ] >>'

5. Start local server
```
./manage.py runserver
```

## Resource
Feel free to use [Glyphish](http://www.glyphish.com/) in the prototype.

## Multiple Mysql Instance
http://www.ducea.com/2009/01/19/running-multiple-instances-of-mysql-on-the-same-machine/

## Recompile Pdf2htmlEx
 - brew install pdf2htmlex
 - cd /Users/huanhuanw/Library/Caches/Homebrew/pdf2htmlex-0.14.6.tar.gz, unpack, modify source
 - tar -zcvf pdf2htmlEX-0.14.6.tar.gz pdf2htmlEX-0.14.6; brew reinstall pdf2htmlex
 - would see Error: SHA256 mismatch; brew edit pdf2htmlex, update the old sha256
 - brew reinstall pdf2htmlex;
 - pdf2htmlEx --zoom 1 --optimize-text 1 --space-as-offset 1 --process-outline 0 --process-form 1 test.pdf test.html

## Fix PDFTK for non-english character
def smart_encode_str(s):
    """Create a UTF-16 encoded PDF string literal for `s`."""
    s = s.decode('utf-8')