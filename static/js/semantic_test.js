
requirejs(['semantic'], function(semantic) {
    $('.accordion').accordion({
        selector: {
            trigger: '.title .icon'
        }
    });
});