define(["ProtoBuf"], function(ProtoBuf) {
    return ProtoBuf.newBuilder({})['import']({
        "package": null,
        "messages": [
            {
                "name": "Source",
                "fields": [
                    {
                        "rule": "optional",
                        "type": "int32",
                        "name": "width",
                        "id": 1
                    },
                    {
                        "rule": "optional",
                        "type": "int32",
                        "name": "height",
                        "id": 2
                    },
                    {
                        "rule": "optional",
                        "type": "FILETYPE",
                        "name": "file_type",
                        "id": 3
                    },
                    {
                        "rule": "repeated",
                        "type": "bytes",
                        "name": "file_content",
                        "id": 4
                    }
                ],
                "enums": [
                    {
                        "name": "FILETYPE",
                        "values": [
                            {
                                "name": "UNKNOWN",
                                "id": 0
                            },
                            {
                                "name": "PDF",
                                "id": 1
                            },
                            {
                                "name": "JPEG",
                                "id": 2
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Tags",
                "fields": [
                    {
                        "rule": "optional",
                        "type": "Source",
                        "name": "original_file",
                        "id": 1
                    },
                    {
                        "rule": "repeated",
                        "type": "Tag",
                        "name": "tags",
                        "id": 2
                    }
                ],
                "messages": [
                    {
                        "name": "Tag",
                        "fields": [
                            {
                                "rule": "optional",
                                "type": "Point",
                                "name": "left_top",
                                "id": 1
                            },
                            {
                                "rule": "optional",
                                "type": "Point",
                                "name": "right_bottom",
                                "id": 2
                            },
                            {
                                "rule": "optional",
                                "type": "string",
                                "name": "value",
                                "id": 3
                            },
                            {
                                "rule": "optional",
                                "type": "int32",
                                "name": "index",
                                "id": 4
                            },
                            {
                                "rule": "optional",
                                "type": "int32",
                                "name": "pdf_width",
                                "id": 5
                            },
                            {
                                "rule": "optional",
                                "type": "int32",
                                "name": "pdf_height",
                                "id": 6
                            },
                            {
                                "rule": "optional",
                                "type": "int32",
                                "name": "tag_type",
                                "id": 7
                            }
                        ],
                        "messages": [
                            {
                                "name": "Point",
                                "fields": [
                                    {
                                        "rule": "optional",
                                        "type": "int32",
                                        "name": "x",
                                        "id": 1
                                    },
                                    {
                                        "rule": "optional",
                                        "type": "int32",
                                        "name": "y",
                                        "id": 2
                                    },
                                    {
                                        "rule": "optional",
                                        "type": "float",
                                        "name": "x_ratio",
                                        "id": 3
                                    },
                                    {
                                        "rule": "optional",
                                        "type": "float",
                                        "name": "y_ratio",
                                        "id": 4
                                    },
                                    {
                                        "rule": "optional",
                                        "type": "string",
                                        "name": "text",
                                        "id": 5
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }).build();
});