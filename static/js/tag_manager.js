define(['tag', 'utils'], function(Tag, utils) {
    return function() {
        var self = this;

        this.list = {};
        this.defaultLeftTop = { 0: 0, 1: 0 };
        this.defaultRightBottom = { 0: 0, 1: 0 };
        this.defaultTagValue = '#TemplateName';

        this.tags = [];

        this.types = ['copy', 'font', 'checkmark'];

        this.appendTag = function(tag) {
            self.tags.push(tag);
            var index = tag.index;
            if (this.list[index]) {
                var list = this.list[index];
                if (list.children().length == 0) {
                    var divider = $('<h4 class="ui horizontal divider tag_page_header">Page ' + (index + 1) + '</h4>');
                    list.append(divider);
                }
                var textWrapper = $('<div class="ui transparent input tag_name"></div>');
                var itemIcon = $('<i class="' + this.types[tag.tag_type] + ' icon tag_icon"></i>');
                var text = $('<input type="text" placeholder="Tag name...">').appendTo(textWrapper);
                text.val(tag.value);

                text.on('propertychange input', function(e) {
                    var valueChanged = false;

                    if (e.type == 'propertychange') {
                        valueChanged = e.originalEvent.propertyName == 'value';
                    } else {
                        valueChanged = true;
                    }
                    if (valueChanged) {
                        /* Code goes here */
                        tag.value = this.value;
                        tag.rectangle.updateContent();
                    }
                });

                var button = $('<div class="tag_close_button"><i class="remove icon"></i></div>');

                button.on('click', function() {
                    self.removeTag(tag);
                });

                var item = $('<div class="tag"></div>')

                item.append(itemIcon);
                item.append(textWrapper);
                item.append(button);
                item.appendTo(list);

                tag.listText = text;
                tag.listItem = item;
            }
        }

        this.removeTag = function(tag) {
            utils.removeFromArray(self.tags, tag);
            if (tag.rectangle) {
                tag.rectangle.remove();
            }
            if (tag.listItem) {
                if (tag.listItem.parent().children().length == 2) {
                    tag.listItem.parent().children()[0].remove();
                }
                
                tag.listItem.remove();
            }
        }

        this.updateTag = function(tag) {
            console.log('tag updated');
            if (tag.listText) {
                tag.listText.val(tag.value);
            }
            if (tag.rectangle) {
            	tag.rectangle.updateContent();
            }
        }

        this.addTag = function(tag) {
            if (tag.tag_type == undefined) {
                tag.tag_type = 0;
            }
            tag.manager = self;
            self.appendTag(tag);
            return tag;
        }

        this.newDefaultTagInManager = function(pageNumber) {
            var tag = new Tag.Tags.Tag();
            tag.left_top = new Tag.Tags.Tag.Point();
            tag.right_bottom = new Tag.Tags.Tag.Point();
            tag.left_top.x = self.defaultLeftTop[0];
            tag.left_top.y = self.defaultLeftTop[1];
            tag.right_bottom.x = self.defaultRightBottom[0];
            tag.right_bottom.y = self.defaultRightBottom[1];
            tag.value = self.defaultTagValue;
            tag.index = pageNumber;
            tag.tag_type = window.tagType;

            tag.manager = self;

            self.appendTag(tag);

            return tag;
        }
    };
});
