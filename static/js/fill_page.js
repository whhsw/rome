require.config({
    paths: {
        'bytebuffer': 'bytebuffer.min',
        'd3': 'd3.v3.min',
        'jquery': 'jquery.min',
        'jspdf': 'jspdf.min.js',
        'long': 'long.min',
        'pdfjs-dist/build/pdf': 'pdfjs-dist/build/pdf',
        'ProtoBuf': 'protobuf.min'
    }
});

requirejs(['d3', 'jquery', 'pdfjs-dist/build/pdf', 'rectangle', 'tag', 'tag_manager'], function(d3, jquery, PDFJS, Rectangle, Tag, TagManager) {
    console.log('js loaded');
});
