define(function() {
    var obj = `{
        "original_file": null,
        "tags": [{
            "left_top": {
                "x": 280,
                "y": 322
            },
            "right_bottom": {
                "x": 608,
                "y": 346
            },
            "value": "#EnglishFamilyName"
        }, {
            "left_top": {
                "x": 669,
                "y": 322
            },
            "right_bottom": {
                "x": 833,
                "y": 346
            },
            "value": "#ChineseFamilyName"
        }, {
            "left_top": {
                "x": 280,
                "y": 354
            },
            "right_bottom": {
                "x": 608,
                "y": 375
            },
            "value": "#EnglishGivenName"
        }, {
            "left_top": {
                "x": 670,
                "y": 353
            },
            "right_bottom": {
                "x": 833,
                "y": 375
            },
            "value": "#ChineseGivenName"
        }, {
            "left_top": {
                "x": 244,
                "y": 383
            },
            "right_bottom": {
                "x": 609,
                "y": 405
            },
            "value": "#EnglishUsedName"
        }, {
            "left_top": {
                "x": 671,
                "y": 384
            },
            "right_bottom": {
                "x": 834,
                "y": 406
            },
            "value": "#ChineseUsedName"
        }, {
            "left_top": {
                "x": 165,
                "y": 417
            },
            "right_bottom": {
                "x": 299,
                "y": 440
            },
            "value": "#Birthday"
        }, {
            "left_top": {
                "x": 375,
                "y": 417
            },
            "right_bottom": {
                "x": 833,
                "y": 440
            },
            "value": "#BirthLocation"
        }]
    }`;
    return function() {
    	return obj;
    }
});
