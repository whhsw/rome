$(function(){
  var currencies = [
    { value: 'Japan', data: 'JPY' },
    { value: 'Jamaica', data: 'JMD' },
    { value: 'Jordan', data: 'JOD' },
    { value: 'Italy', data: 'JPY' },
    { value: 'Australia', data: 'JPY' },
    { value: 'Thailand', data: 'AFN' },
    { value: 'South Korea', data: 'ALL' },
    { value: 'North Korea', data: 'DZD' },
    { value: 'France', data: 'EUR' },
    { value: 'Fiji', data: 'JMD' },
    { value: 'Tajikistan', data: 'JMD' },
  ];
  
  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete({
    lookup: currencies,
    onSelect: function (suggestion) {
      $('#choose_template').show();
    }
  });
});