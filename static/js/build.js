({
    baseUrl: ".",
    paths: {
        'bytebuffer': 'bytebuffer.min',
        'd3': 'd3.v3.min',
        'jquery': 'jquery.min',
        'jspdf': 'jspdf.min.js',
        'long': 'long.min',
        'pdfjs-dist/build/pdf': 'pdfjs-dist/build/pdf',
        'ProtoBuf': 'protobuf.min'
    },
    name: "main",
    out: "main-built.js"
})