require.config({
    baseUrl: 'js',
    paths: {
        'bytebuffer': 'bytebuffer.min',
        'd3': 'd3.v3.min',
        'jquery': 'jquery.min',
        'jspdf': 'jspdf.min.js',
        'long': 'long.min',
        'semantic': 'semantic.min',
        'pdfjs-dist/build/pdf': 'pdfjs-dist/build/pdf',
        'ProtoBuf': 'protobuf.min'
    },
    shim: {
        'semantic': {
            deps: ["jquery"],
            exports: "$",
            init: function($) {
                return {
                    "$.fn.accordion": $.fn.accordion,
                    "$.fn.accordion.settings": $.fn.accordion.settings,
                    "$.fn.sticky": $.fn.sticky,
                };
            }
        }
    }
});