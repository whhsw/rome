require.config({
    paths: {
        'bytebuffer': 'bytebuffer.min',
        'd3': 'd3.v3.min',
        'jquery': 'jquery.min',
        'jspdf': 'jspdf.min',
        'long': 'long.min',
        'pdfjs-dist/build/pdf': 'pdfjs-dist/build/pdf',
        'ProtoBuf': 'protobuf.min'
    }
});

requirejs(['d3', 'jquery', 'jspdf', 'pdfjs-dist/build/pdf', 'rectangle', 'tag', 'tag_manager', 'utils', 'fake_tag', 'ProtoBuf'], function(d3, jquery, jspdf, PDFJS, Rectangle, Tag, TagManager, utils, fake_tag, ProtoBuf) {
    var url = '../static/test/pdf/visa_japan.pdf';
    var tagManager = new TagManager();

    var tags = Tag.Tags.decodeJSON(fake_tag());

    var iframe = document.getElementById('iframe');

    utils.PDFToImages(url, function(images) {
        
        if (images.length > 0) {
            var image = new Image();
            image.src = images[0];

            var width = image.width/1.8;
            var height = image.height/1.8;

            var pageNumber = 0;
            var doc = new jsPDF('p', 'px', [width, height]);
            doc.addImage(image, 'JPEG', 0, 0);

            for (var i = 0; i < tags.tags.length; i++) {
                var tag = tags.tags[i];
                doc.text(tag.left_top.x/1.35, tag.right_bottom.y/1.35, tag.value);
            }


            for (var i = 1; i < images.length; i++) {
                doc.addPage();
                var image = new Image();
                image.src = images[i];
                doc.addImage(image, 'JPEG', 0, 0);
            }
            var string = doc.output('datauristring');
            $('iframe').attr('src', string);
        }
    });
});
