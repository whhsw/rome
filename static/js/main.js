require.config({
    paths: {
        'bytebuffer': 'bytebuffer.min',
        'd3': 'd3.v3.min',
        'jquery': 'jquery.min',
        'jspdf': 'jspdf.min.js',
        'long': 'long.min',
        'pdfjs-dist/build/pdf': 'pdfjs-dist/build/pdf',
        'ProtoBuf': 'protobuf.min'
    }
});

requirejs(['d3', 'jquery', 'pdfjs-dist/build/pdf', 'rectangle', 'tag', 'tag_manager'], function(d3, jquery, PDFJS, Rectangle, Tag, TagManager) {
    var url = window.pdf;

    PDFJS.PDFJS.verbosity = PDFJS.PDFJS.VERBOSITY_LEVELS.errors;

    PDFJS.workerSrc = '../pdf.worker.js';

    var tagManager = new TagManager();

    var pdfWidth = 0;
    var pdfHeight = 0;

    $('#save_button').on('click', function() {

        $('#dimmer').addClass('active');
        $('#dimmer_label').text('Saving...');

        var tagsObject = new Tag.Tags();
        var tagArray = tagManager.tags;
        for (var i = 0; i < tagArray.length; i++) {
            
            var tag = new Tag.Tags.Tag();

            tag.left_top = tagArray[i].left_top;
            tag.right_bottom = tagArray[i].right_bottom;
            tag.value = tagArray[i].value;
            tag.index = tagArray[i].index;
            tag.tag_type = tagArray[i].tag_type;

            tagsObject.tags.push(tag);
        }
        tagsObject['pdf_width'] = pdfWidth;
        tagsObject['pdf_height'] = pdfHeight; 
        tagsObject['template_id'] = window.formId; 
        $.ajax({
            type: "POST",
            url: "/collect_tags",
            dataType: "json",
            data: tagsObject.encodeJSON(),
            beforeSend: function(xhr, settings) {
                if (!this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", window.csrf);
                }
            },
            success: function (data) {
                $('#dimmer_label').text('Save success!');
                setTimeout(function() {
                    $('#dimmer').removeClass('active');
                }, 1000);
            },
            error: function (data) {
                $('#dimmer_label').text('Save falied!');
                setTimeout(function() {
                    $('#dimmer').removeClass('active');
                }, 1000);
            }
        });
    });

    var tagsForPages = {};
    if (window.tagString.length != 0) {
        // Try to load json
        var tagObject = Tag.Tags.decodeJSON(window.tagString);
        var tags = tagObject.tags;
        for (var i = 0; i < tags.length; i++) {
            if (!tagsForPages.hasOwnProperty(tags[i].index)) {
                tagsForPages[tags[i].index] = [];
            }
            tagsForPages[tags[i].index].push(tags[i]);
        }
    }

    function resizePages() {
        var pagesContainer = $('#pages');
        var leftContainer = $('.mdl-layout__drawer');
        var rightContainer = $('#right_bar');
        pagesContainer.css({
            'width': $(window).width() - leftContainer.width() - rightContainer.width() - 100 + 'px',
        });

        var sidebar_tag_list = $('#sidebar_tag_list');
        var sidebar_title_container = $('#sidebar_title_container');
        var save_button_container = $('#save_button_container');

        sidebar_tag_list.css({
            'height': $(window).height() - sidebar_title_container.height() - save_button_container.height() + 'px',
        });
    };

    $(window).resize(function() {
        resizePages();
    });

    // Download PDF
    PDFJS.getDocument(url).then(function handlePDF(pdf) {
        var page_count = pdf.numPages;
        var pagesContainer = $('#pages');

        resizePages();

        for (var i = 0; i < page_count; i++) {
            pdf.getPage(i + 1).then(function handlePDFPage(page) {
                var pageNumber = page.pageIndex;

                var pixelRatio = 'devicePixelRatio' in window ? window.devicePixelRatio * 1.4 : 1.4;

                var viewport = page.getViewport(pixelRatio);

                pdfWidth = viewport.width / pixelRatio * 1.4;
                pdfHeight = viewport.height /pixelRatio * 1.4;

                var width = viewport.width * 1.4;
                var height = viewport.height * 1.4;

                var container = $('<div id="container_' + pageNumber + '" class="page"></div>').appendTo(pagesContainer);
                container.css({
                    'width': width / pixelRatio,
                    'height': height / pixelRatio,
                    'position': 'relative',
                    'padding-bottom': '20px',
                    'margin-left': 'auto',
                    'margin-right': 'auto',
                    'overflow': 'hidden'
                });

                // Create canvas and render PDF page into canvas context
                var canvas = $('<canvas id="canvas_' + pageNumber + '"></canvas>').appendTo(container);
                var context = canvas[0].getContext('2d');
                canvas[0].height = height;
                canvas[0].width = width;

                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                page.render(renderContext);

                canvas.css({
                    'transform': 'scale(' + (1 / pixelRatio * 1.4) + ', ' + (1 / pixelRatio * 1.4) + ')',
                    'transformOrigin': '0% 0%',
                })

                $('<svg class="tagview" id="svg_' + pageNumber + '"></svg>').appendTo(container).attr({
                        width: width / pixelRatio,
                        height: height / pixelRatio
                    })
                    .css({
                        'position': 'absolute',
                        'top': '0',
                        'left': '0',
                        'width': width / pixelRatio,
                        'height': height / pixelRatio,
                        'box-shadow': '0px 0px 4px #F0F0F4 inset'
                    });

                var divider = $('<h4 class="ui horizontal divider tag_page_header">' + (pageNumber + 1) + '</h4>').appendTo(pagesContainer);

                var rightBar = $('.sidebar_tag_list');
                var list = $('<div class="tag_in_on_page" id="ul_' + pageNumber + '"></div>').appendTo(rightBar);

                tagManager.list[pageNumber] = list;

                var svg = d3.select('#svg_' + pageNumber);

                if (tagsForPages.hasOwnProperty(pageNumber)) {
                    var tags = tagsForPages[pageNumber];

                    for (var i = 0; i < tags.length; i++) {
                        var rectangle = new Rectangle(svg,
                            tagManager.addTag(tags[i]),
                            tagManager);
                        rectangle.showTag();
                        rectangle.clickDone = true;
                        rectangle.dragDown = true;
                    }
                }

                function makeHandler() {
                    var createState = 0;
                    var lastRectangle = {};
                    var pageNumber_ = pageNumber;
                    var svg_ = d3.select('#svg_' + pageNumber_);

                    function mousedownHandler() {
                        if (createState == 0) {
                            var rectangle = new Rectangle(svg_,
                                tagManager.newDefaultTagInManager(pageNumber_),
                                tagManager);
                            rectangle.svgMouseDown();

                            lastRectangle = rectangle;
                        } else {
                            lastRectangle.svgMouseDown();
                        }

                        createState += 1;
                        createState = createState % 2;
                    };

                    function mousemoveHandler() {
                        if (lastRectangle.hasOwnProperty('svgMouseMove')) {
                            lastRectangle.svgMouseMove();
                        }
                    }
                    return {
                        mousedownHandler: mousedownHandler,
                        mousemoveHandler: mousemoveHandler
                    }
                }

                var handlers = makeHandler();
                d3.select('#svg_' + pageNumber)
                    .on('mousedown', handlers.mousedownHandler)
                    .on('mousemove', handlers.mousemoveHandler);
            });
        }
    });
});
