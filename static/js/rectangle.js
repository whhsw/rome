// Draw a draggable rectangle
define(['d3', 'utils', 'tag'], function(d3, utils, Tag) {
    return function(svg, tag, tagManager) {
        var self = this;

        this.clickDone = false;
        this.dragDown = false;

        this.rect = {};

        this.tag = tag;
        this.tag.rectangle = this;

        this.width = 0;
        this.height = 0;
        this.tagManager = tagManager;

        var dragR = d3.behavior.drag().on('drag', dragRect);
        var dragC1 = d3.behavior.drag().on('drag', dragPoint1);
        var dragC2 = d3.behavior.drag().on('drag', dragPoint2);
        var dragC3 = d3.behavior.drag().on('drag', dragPoint3);
        var dragC4 = d3.behavior.drag().on('drag', dragPoint4);

        function stopMove() {
            if (self.dragDown && self.clickDone) {
                d3.event.stopPropagation();
            }
        }

        this.showTag = function(tag) {
            console.log(this.tag);
            this.createElements();
            this.updateRect();
        }

        this.remove = function() {
            this.group.remove();
        }

        this.updateContent = function() {
            if (this.word) {
                this.word.text(self.tag.value);
            }
        }

        this.createElements = function() {
            this.group = svg.selectAll("x").data([{ title: this.tag.value }]).enter().append('g');
            this.rectangleElement = this.group.append('rect').attr('class', 'rectangle').call(dragR);
            this.pointElement1 = this.group.append('circle').attr('class', 'pointC').call(dragC1);
            this.pointElement2 = this.group.append('circle').attr('class', 'pointC').call(dragC2);
            this.pointElement3 = this.group.append('circle').attr('class', 'pointC').call(dragC3);
            this.pointElement4 = this.group.append('circle').attr('class', 'pointC').call(dragC4);

            if (this.tag.tag_type == 2) {
                this.checkmark = this.group.append('image').attr('xlink:href', '/static/img/checkmark.png');
                this.checkmark.on('mousedown', stopMove);
                this.checkmark.call(dragR);
            }
            else {
                this.word = this.group.append('text').attr("class", "title")
                    .text(function(d) {
                        self.tag.value =  d? d['title'] : this.this.tag.value;
                        return self.tag.value;
                    })
                    .call(utils.makeEditable, 'title', this.tag);
                this.word.on('mousedown', stopMove);
                this.word.call(dragR);
            }

            this.rectangleElement.on('mousedown', stopMove);
            this.pointElement1.on('mousedown', stopMove);
            this.pointElement2.on('mousedown', stopMove);
            this.pointElement3.on('mousedown', stopMove);
            this.pointElement4.on('mousedown', stopMove);
            
        }

        this.svgMouseDown = function() {
            var pos = d3.mouse(svg[0][0]);

            if (!self.clickDone && !self.dragDown) {
                self.tag.left_top.x = pos[0];
                self.tag.left_top.y = pos[1];
                self.tag.right_bottom.x = pos[0];
                self.tag.right_bottom.y = pos[1];
                self.showTag();

                self.clickDone = true;
            } else {
                self.dragDown = true;
            }
        }

        this.svgMouseMove = function() {
            var pos = d3.mouse(svg[0][0]);

            if (self.clickDone && !self.dragDown) {
                self.tag.right_bottom.x = pos[0];
                self.tag.right_bottom.y = pos[1];
                self.updateRect();
            }
        }

        this.updateRect = function() {

            this.rect = this.rectangleElement;
            this.rect.attr({
                x: this.tag.right_bottom.x - this.tag.left_top.x > 0 ? this.tag.left_top.x : this.tag.right_bottom.x,
                y: this.tag.right_bottom.y - this.tag.left_top.y > 0 ? this.tag.left_top.y : this.tag.right_bottom.y,
                width: Math.abs(this.tag.right_bottom.x - this.tag.left_top.x),
                height: Math.abs(this.tag.right_bottom.y - this.tag.left_top.y)
            });

            this.pointElement1.attr('r', 3)
                .attr('cx', this.tag.left_top.x)
                .attr('cy', this.tag.left_top.y);
            this.pointElement2.attr('r', 3)
                .attr('cx', this.tag.right_bottom.x)
                .attr('cy', this.tag.right_bottom.y);
            this.pointElement3.attr('r', 3)
                .attr('cx', this.tag.right_bottom.x)
                .attr('cy', this.tag.left_top.y);
            this.pointElement4.attr('r', 3)
                .attr('cx', this.tag.left_top.x)
                .attr('cy', this.tag.right_bottom.y);

            if (this.word) {
                this.word.attr('x', Math.min(this.tag.left_top.x, this.tag.right_bottom.x) + 3)
                    .attr('y', (this.tag.right_bottom.y + this.tag.left_top.y) / 2 + 5);
            }

            if (this.checkmark) {
                this.checkmark.attr('x', Math.min(this.tag.left_top.x, this.tag.right_bottom.x))
                this.checkmark.attr('y', Math.min(this.tag.left_top.y, this.tag.right_bottom.y));
                this.checkmark.attr('width', Math.abs(this.tag.right_bottom.x - this.tag.left_top.x));
                this.checkmark.attr('height', Math.abs(this.tag.right_bottom.y - this.tag.left_top.y));
            }
        }

        function dragRect() {
            var e = d3.event;

            self.tag.left_top.x += e.dx;
            self.tag.left_top.y += e.dy;
            self.tag.right_bottom.x += e.dx;
            self.tag.right_bottom.y += e.dy;

            self.rect.style('cursor', 'move');
            self.updateRect();
        }

        function dragPoint1() {
            var e = d3.event;

            self.tag.left_top.x += e.dx;
            self.tag.left_top.y += e.dy;

            self.updateRect();
        }

        function dragPoint2() {
            var e = d3.event;

            self.tag.right_bottom.x += e.dx;
            self.tag.right_bottom.y += e.dy;

            self.updateRect();
        }

        function dragPoint3() {
            var e = d3.event;

            self.tag.right_bottom.x += e.dx;
            self.tag.left_top.y += e.dy;

            self.updateRect();
        }

        function dragPoint4() {
            var e = d3.event;

            self.tag.left_top.x += e.dx;
            self.tag.right_bottom.y += e.dy;

            self.updateRect();
        }
    };
});
