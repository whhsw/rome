define(['d3', 'jquery', 'pdfjs-dist/build/pdf'], function(d3, jquery, PDFJS) {
    function makeEditable(d, field, tag) {
        this.on("mouseover", function() {
                d3.select(this).style("fill", "red");
            })
            .on("mouseout", function() {
                d3.select(this).style("fill", null);
            })
            .on("click", function(d) {
                // Todo(jswang): more accurate way to access the parent svg element.
                var p = this.parentNode.parentNode;

                var xy = this.getBoundingClientRect();
                var p_xy = p.getBoundingClientRect();

                var x = xy.left - p_xy.left;
                var y = xy.top - p_xy.top;

                var el = d3.select(this);
                var p_el = d3.select(p);

                var frm = p_el.append("foreignObject");

                var width = Math.abs(tag.left_top.x - tag.right_bottom.x);

                d[field] = tag.value;

                var inp = frm
                    .attr("x", x)
                    .attr("y", y)
                    .attr("width", width)
                    .attr("height", 25)
                    .append("xhtml:form")
                    .append("input")
                    .attr("value", function() {
                        this.focus();
                        return d[field];
                    })
                    .attr("style", "width: " + (width - 10) + 'px;')
                    .on("blur", function() {
                        var txt = inp.node().value;
                        d[field] = txt;
                        el.text(function(d) {
                            tag.value = d[field];
                            tag.manager.updateTag(tag);
                            return d[field];
                        });

                        p_el.select("foreignObject").remove();
                    })
                    .on('mousedown', function() {
                        d3.event.stopPropagation();
                    })
                    .on("keypress", function() {

                        if (!d3.event)
                            d3.event = window.event;

                        var e = d3.event;
                        if (e.keyCode == 13) {
                            if (typeof(e.cancelBubble) !== 'undefined') // IE
                                e.cancelBubble = true;
                            if (e.stopPropagation)
                                e.stopPropagation();
                            e.preventDefault();

                            var txt = inp.node().value;

                            d[field] = txt;
                            el.text(function(d) {
                                tag.value = d[field];
                                tag.manager.updateTag(tag);
                                return d[field];
                            });

                            p_el.select("foreignObject").remove();
                        }
                    });
            });
    };

    function removeFromArray(arr, item) {
        for (var i = arr.length; i--;) {
            if (arr[i] === item) {
                arr.splice(i, 1);
            }
        }
    }

    function PDFToImages(filePath, callback) {
        function Num(num) {
            var num = num;

            return function() {
                return num;
            }
        };

        function renderPDF(url, options) {
            var options = options || {
                    scale: 2
                },
                func,
                pdfDoc,
                def = $.Deferred(),
                promise = $.Deferred().resolve().promise(),
                width,
                height,
                makeRunner = function(func, args) {
                    return function() {
                        return func.call(null, args);
                    };
                };

            function renderPage(num) {
                var def = $.Deferred(),
                    currPageNum = new Num(num);
                pdfDoc.getPage(currPageNum()).then(function(page) {
                    var viewport = page.getViewport(options.scale);
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext('2d');
                    var renderContext = {
                        canvasContext: ctx,
                        viewport: viewport
                    };

                    if (currPageNum() === 1) {
                        height = viewport.height;
                        width = viewport.width;
                    }

                    canvas.height = height;
                    canvas.width = width;

                    page.render(renderContext).then(function() {
                        var image = canvas.toDataURL("image/jpeg", 1.0);
                        images.push(image);
                        def.resolve();
                    });
                })

                return def.promise();
            }

            function renderPages(data) {
                pdfDoc = data;

                var pagesCount = pdfDoc.numPages;
                for (var i = 1; i <= pagesCount; i++) {
                    func = renderPage;
                    promise = promise.then(makeRunner(func, i));
                }
                promise = promise.then(function() {
                    callback(images);
                });
            }

            PDFJS.getDocument(url).then(renderPages);
        };

        var images = [];

        PDFJS.PDFJS.verbosity = PDFJS.PDFJS.VERBOSITY_LEVELS.errors;
        PDFJS.workerSrc = '../pdf.worker.js';

        renderPDF(filePath);
    }

    return {
        makeEditable: makeEditable,
        removeFromArray: removeFromArray,
        PDFToImages: PDFToImages
    }
});
