$(document).ready(function() {

  //////////////////////////////////////////////////////////////////////////////
  // Try Now button transition
  //////////////////////////////////////////////////////////////////////////////

  $(document).on("click", "#buttonTryNow", function(){
    var welcomeItems = $(".welcome").find(".logo, .slogan, .actions");
    welcomeItems.addClass("hide");
    var loginButtons = $(".welcome .login-buttons");
    setTimeout(function(){
      welcomeItems.css("display", "none");
      loginButtons.css("display", "block");
    }, 501);
    setTimeout(function(){
      loginButtons.removeClass("hide");
    }, 550);
  });


  //////////////////////////////////////////////////////////////////////////////
  // Dropdown Component
  //////////////////////////////////////////////////////////////////////////////
  $(".dropdown").each(function(){
    var container = $(this);
    var current = container.find(".current");
    var menu = container.find(".menu");
    var children = container.find(".menu li");
    var originalWidth = current.outerWidth(true) + 1;
    var totalMaxWidth = Math.max(Math.max.apply(null, children.map(function(){
      return $(this).outerWidth(true);
    }).get()), originalWidth);
    var menuMaxWidth = Math.max(Math.max.apply(null, children.map(function(){
      return $(this).outerWidth(true);
    }).get()), originalWidth);
    var listHeight = menu.find("li").length * 40 + 40 + 2;
    container.attr({
      "originalWidth": originalWidth,
      "maxWidth": totalMaxWidth,
      "listHeight": listHeight
    });
    container.css("width", originalWidth);
    container.css({
      "width": originalWidth
    });
    menu.css({
      "width": originalWidth,
      "height": 40
    });
  });

  $(document).on("click", ".dropdown", function(){
    var container = $(this);
    var menu = container.find(".menu");
    var current = container.find(".current");
    var listHeight = container.attr("listHeight");
    if (container.hasClass("open")) {
      var originalWidth = container.attr("originalWidth");
      container.toggleClass("open");
      container.css({
        "width": originalWidth
      });
      menu.css({
        "width": originalWidth,
        "height": 40
      });
    } else {
      var maxWidth = container.attr("maxWidth");
      container.toggleClass("open");
      container.css({
        "width": maxWidth
      });
      menu.css({
        "width": maxWidth,
        "height": listHeight
      });
    }
  });

  $(document).on("click", function(e){
    if(!$(e.target).closest('.dropdown').length) {
      $('.dropdown').each(function(){
        var container = $(this);
        var menu = container.find(".menu");
        var originalWidth = container.attr("originalWidth");
        container.removeClass("open");
        container.css({
          "width": originalWidth
        });
        menu.css({
          "width": originalWidth,
          "height": 40
        });
      });
    }
  });


  //////////////////////////////////////////////////////////////////////////////
  // Checkbox Component
  //////////////////////////////////////////////////////////////////////////////

  $(document).on("blur", "input[type=text]", function(){
    if ($(this).val().length) {
      $(this).addClass("has-content");
    } else {
      $(this).removeClass("has-content");
    }
  });


  //////////////////////////////////////////////////////////////////////////////
  // Add to / Remove from print bucket
  //////////////////////////////////////////////////////////////////////////////

  $(document).on("click", ".required-documents input[type='checkbox'] + label", function(){
    var label = $(this);
    var checked = label.siblings("input[type='checkbox']:checked");
    var labelContent = label.html();
    var fileList = $(".print-bucket .file-list");
    var docID = label.attr("for");
    var printID = docID.replace("doc-", "print-");
    var printFile = "#" + printID;
    if (!(checked.length)) {
      var li = $("<li/>")
        .attr("id", printID)
        .addClass("hide")
        .appendTo(fileList);
      var fileName = $("<div/>")
        .addClass("file-name")
        .html(labelContent)
        .appendTo(li);
      var closeButton = $("<div/>")
        .addClass("close-button")
        .html("<svg width='14px' height='14px'><polygon points='14 1.41 12.59 0 7 5.59 1.41 0 0 1.41 5.59 7 0 12.59 1.41 14 7 8.41 12.59 14 14 12.59 8.41 7'></polygon></svg>")
        .appendTo(li);
      setTimeout(function(){
        fileList.find(printFile).removeClass("hide");
      }, 50);
    } else {
      var file = fileList.find(printFile);
      file.addClass("hide");
      setTimeout(function(){
        file.remove();
      }, 300);
    }
  });

  $(document).on("click", ".close-button", function(){
    var file = $(this).parent();
    file.addClass("hide");
    setTimeout(function(){
      file.remove();
    }, 300);
  });


});
