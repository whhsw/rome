def upload(request):
    if not request.POST:
        return HttpResponseBadRequest()
    upload_type = request.POST.get('upload_type', '')

    if upload_type == 'document_with_field':
        record, created = ApplicationRecord.objects.get_or_create(
                application_id=request.POST["application_id"], 
                field_id=request.POST["field_id"])
        if record.myfile:
            record.myfile.file = request.FILES['file'] 
        else:
            record.myfile = MyFile.objects.create(file=request.FILES["file"])
        record.myfile.save()
        record.myfile.create_thumbnail()
        record.save()
        return HttpResponse(json.dumps({'success': True, 'url': record.myfile.file.url}), content_type='application/json')  


def form_fill(application, input_images_path, input_tags, output_file):
    image_names = sorted(
            [os.path.join(input_images_path, fn) for fn in os.listdir(input_images_path) if fn.endswith('jpg')],
            key=lambda path: int(re.search(r'-(\d+)\.jpg$', path).group(1)))
    if len(image_names) == 0:
        return
    # Get proper size.
    image_width, image_height = ImageReader(image_names[0]).getSize()
    width, height = letter
    image_pdf_ratio = min(width / image_width, height / image_height)
    width = image_width * image_pdf_ratio
    height = image_height * image_pdf_ratio

    # print width, height
    can = canvas.Canvas(output_file, pagesize=(width, height))
    for page_num in range(len(image_names)):
        page = ImageReader(image_names[page_num])
        can.drawImage(page, 0, 0, width, height)
        for tag_meta in input_tags:
            if tag_meta.field_meta != "":
                # print tag_meta.field_meta
                tag_cat = json.loads(tag_meta.field_meta)
                for tag_key in tag_cat:
                    tag = tag_cat[tag_key]
                    if tag['index'] == page_num: 
                        ltx = float(min(tag['left_top']['x'], tag['right_bottom']['x']))
                        lty = float(min(tag['left_top']['y'], tag['right_bottom']['y']))
                        rbx = float(max(tag['left_top']['x'], tag['right_bottom']['x']))
                        rby = float(max(tag['left_top']['y'], tag['right_bottom']['y']))
                        # print tag['pdf_width'], tag['pdf_height'], width, height
                        w = (rbx - ltx) / tag['pdf_width'] * width;
                        h = (rby - lty) / tag['pdf_height'] * height;
                        lx = (ltx / tag['pdf_width']) * width;
                        ly = (1 - rby / tag['pdf_height']) * height;
                        if tag['tag_type'] == 1:
                            # text
                            pdfform.textFieldAbsolute(can, "predefined_" + tag_key, lx, ly, w, h, value=tag['value'])
                        elif tag['tag_type'] == 2:
                            # checkbox
                            pdfform.buttonFieldAbsolute(can, "predefined_" + tag_key, 'Yes', lx, ly, w, h)
                        elif tag['tag_type'] == 0:
                            # slot
                            value = tag['value'].split(':')
                            field = Field.objects.get(key=value[0])
                            try:
                                application_record = ApplicationRecord.objects.get(application=application, field=field)
                                application_record_value = application_record.value
                            except ApplicationRecord.DoesNotExist:
                                historical_data = ProfileHistoricalData.objects.get(field=field, profile=application.profile)
                                application_record_value = historical_data.value
                            except ProfileHistoricalData.DoesNotExist:
                                application_record_value = ''

                            if len(value) == 1:
                                pdfform.textFieldAbsolute(can, tag['value'], lx, ly, w, h, value=application_record_value)
                            else:
                                if tag_key == application_record_value:
                                    pdfform.buttonFieldAbsolute(can, tag['value'], 'Yes', lx, ly, w, h)
                                else:
                                    pdfform.buttonFieldAbsolute(can, tag['value'], 'Off', lx, ly, w, h)
        can.showPage()
    can.save()

def combinePDFandFill():
    records = ApplicationRecord.objects.filter(application=application)
    forms = ApplicationStructure.objects \
        .filter(application=application, template__type='form') 

    output = PdfFileWriter()
    files_to_print = records.filter(value='on').exclude(myfile=None)
    for record in files_to_print:
        append_pdf(PdfFileReader(file(record.myfile.pdf, "rb")), output)

    for form in forms:
        template_structure = TemplateStructure.objects.filter(template=form.template)
        filled_pdf_file = os.path.join(
            settings.LOCAL_APPLICATION_STORAGE, 
            "%s_%s_filled.pdf" % (application_id, form.template_id))
        form_fill(application, form.template.myfile.img, template_structure, filled_pdf_file) 
        append_pdf(PdfFileReader(file(filled_pdf_file, "rb")), output)

    final_file = os.path.join(settings.LOCAL_APPLICATION_STORAGE, str(application_id) + '.pdf')
    output.write(file(final_file, "wb"))

    tmp = open(final_file)
    application.output_file.save(final_file, File(tmp))
    tmp.close()
    
    return HttpResponse('done')