# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-03 10:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0013_auto_20160803_1002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicationrecord',
            name='thumbnail',
            field=models.ImageField(blank=True, default='', null=True, upload_to='uploads/thumbnail/'),
        ),
    ]
