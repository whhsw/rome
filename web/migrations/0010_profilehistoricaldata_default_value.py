# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-03 06:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0009_auto_20160803_0516'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilehistoricaldata',
            name='default_value',
            field=models.TextField(blank=True, default=''),
        ),
    ]
