# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-05 06:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0025_myfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='template',
            name='myfile',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='web.MyFile'),
        ),
    ]
