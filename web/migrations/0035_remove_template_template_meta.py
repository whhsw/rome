# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-14 15:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0034_template_template_meta'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='template',
            name='template_meta',
        ),
    ]
