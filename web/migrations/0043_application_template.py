# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-23 15:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0042_auto_20160821_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='template',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='web.Template'),
            preserve_default=False,
        ),
    ]
