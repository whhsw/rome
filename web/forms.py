from django import forms
from allauth.account.forms import SetPasswordField

class RegistrationForm(forms.Form):
    password = SetPasswordField(min_length=8, max_length=16, label=("Password"))

    def signup(self, request, user):
        user.set_password(self.cleaned_data["password"])
        user.save()