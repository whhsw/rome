# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.dispatch import receiver
from django.core.files import File
from django.db.models.signals import post_save
from guardian.shortcuts import assign_perm
from wand.image import Image as WandImage
import subprocess
import os
import urllib
import shutil
import datetime
import pdf
import json

class BaseModel(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, null=False)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

ATTACHMENT_UPLOAD_TO = getattr(
    settings,
    'ATTACHMENT_UPLOAD_TO',
    'uploads/%Y/%m/%d/'
)

FIELD_TYPE_CHOICES = (
    ('file', 'File'),
    ('text', 'Text'),
    ('digit', 'Digit'),
    ('date', 'Date'),
    ('single_choice', 'Single Choice'),
    ('multi_choice', 'Multi Choice'),
)

TEMPLATE_TYPE_CHOICES = (
    ('form', '表格'),
    ('checklist', '清单'),
)

FIELD_CATEGORY_CHOICES = (
    ('basic', '基本信息'),
    ('job', '职业信息'),
    ('passport', '护照'),
    ('family', '家庭信息'),
    ('trip', '行程相关'),
    ('security', '安全信息'),
    ('misc', '其他'),
)


def modification_datetime(filename):
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)

class MyFile(BaseModel):
    file = models.FileField(upload_to=ATTACHMENT_UPLOAD_TO, default=None)
    thumbnail = models.ImageField(upload_to='uploads/thumbnail/', blank=True, null=True, default='')
    pdf_local = models.TextField(default="", blank=True)
    img_local = models.TextField(default="", blank=True)

    def save(self, *args, **kwargs):
        self.pdf_local = os.path.join(settings.LOCAL_FILE_STORAGE, str(self.id) + '.pdf')

        if self.extension == '.pdf':
            self.img_local = os.path.join(settings.LOCAL_FILE_STORAGE, str(self.id) + '_pngs')
        else:
            self.img_local = os.path.join(settings.LOCAL_FILE_STORAGE, str(self.id) + '.png')
        super(MyFile, self).save()

    def create_thumbnail(self):
        thumbnail_local = os.path.join(settings.LOCAL_THUMB_STORAGE, str(self.id) + '.png')
        command = "convert -quality 95 -thumbnail 222 %s[0] %s" % (self.file.url, thumbnail_local)
        subprocess.call(command.split(' ')) 
        file = open(thumbnail_local)
        self.thumbnail.save(str(self.id) + '.png', File(file))
        file.close()

    @property
    def pdf(self):
        # Update local file when it's not there or it's outdated:
        if not os.path.exists(self.pdf_local) or float(self.update_time.strftime("%s")) > os.path.getmtime(self.pdf_local):
            if self.extension == '.pdf':
                urllib.urlretrieve(self.file.url, self.pdf_local)
            else:
                subprocess.call(["convert", self.file.url, self.pdf_local])
        return self.pdf_local   

    @property
    def img(self):
        if not os.path.exists(self.img_local) or float(self.update_time.strftime("%s")) > os.path.getmtime(self.img_local):
            if self.extension == '.pdf':
                if os.path.exists(self.img_local):
                    shutil.rmtree(self.img_local)
                os.makedirs(self.img_local)
                im = WandImage(filename=self.file.url, resolution=400)
                im.save(filename=os.path.join(self.img_local, 'pre.jpg'))
            else:
                urllib.urlretrieve(self.file.url, self.img_local)
        return self.img_local

    @property
    def _html_path(self):
        return os.path.join(settings.LOCAL_FILE_STORAGE, str(self.id) + '.html')

    @property
    def html(self):
        if not os.path.exists(self._html_path) or float(self.update_time.strftime("%s")) > os.path.getmtime(self._html_path):
            pdf.Pdf2Html(self.pdf, settings.LOCAL_FILE_STORAGE, str(self.id) + '.html')
        return self._html_path

    @property
    def extension(self):
        return os.path.splitext(self.file.name)[1]

    def __unicode__(self):
        return self.file.name

class Field(BaseModel):
    key = models.CharField(max_length=100, default="")
    description = models.CharField(max_length=100, default="")
    type = models.CharField(max_length=20, choices=FIELD_TYPE_CHOICES)
    options = models.TextField(default="", blank=True)
    category = models.CharField(max_length=20, choices=FIELD_CATEGORY_CHOICES, default='misc')
    tooltip = models.CharField(max_length=100, default="", blank=True)
    show_in_profile = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s - %s - %s - %s' % (self.key , self.description, self.type, self.show_in_profile)

class Template(BaseModel):
    name = models.CharField(max_length=100, default="")
    myfile = models.ForeignKey(MyFile, blank=True, default=None, null=True)
    language = models.CharField(max_length=20, default="en")
    type = models.CharField(max_length=20, choices=TEMPLATE_TYPE_CHOICES)

    def __unicode__(self):
        return '%s - %s' % (self.name, self.type)

class TagMap(models.Model):
    tag = models.CharField(max_length=100)
    template = models.ForeignKey(Template)
    field = models.ForeignKey(Field, null=True)
    identifier = models.CharField(max_length=100, default=None, null=True)


class TemplateStructure(BaseModel):
    template = models.ForeignKey(Template)
    field = models.ForeignKey(Field)
    field_meta = models.TextField(default="")
    number_of_copy = models.IntegerField(default=1)
    priority = models.IntegerField(default=0)
    required = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s - %s - %s - %s' % (self.template, self.field, self.priority, self.required)

class Profile(BaseModel):
    alias = models.CharField(max_length=100, default="")
    owner = models.ForeignKey(User)
    # whether or not this the default Myself profile for user
    default = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ('view_profile', 'View Profile'),
            ('edit_profile', 'Edit Profile'),
        )

    def __unicode__(self):
        return '%s - %s' % (self.alias, self.owner)

class Application(BaseModel):
    alias = models.CharField(max_length=100, default="")
    owner = models.ForeignKey(User)
    profile = models.ForeignKey(Profile)
    template = models.ForeignKey(Template)
    # For api v2
    output_data = models.TextField(default="")
    # For api v1
    output_file = models.FileField(upload_to=ATTACHMENT_UPLOAD_TO, blank=True)
    api_version = models.IntegerField(default=2)

    class Meta:
        permissions = (
            ('view_application', 'View Application'),
            ('edit_application', 'Edit Application'),
        )

    def __unicode__(self):
        return '%s - %s' % (self.alias, self.profile)

    @property
    def alias_display(self):
        return self.alias if self.alias else '未命名 - ' + self.template.name
    

class ShareToken(BaseModel):
    application = models.ForeignKey(Application)
    owner = models.ForeignKey(User)
    token = models.CharField(max_length=64, unique=True)

class ProfileHistoricalData(BaseModel):
    field = models.ForeignKey(Field)
    profile = models.ForeignKey(Profile)
    value = models.TextField(default="", blank=True)
    myfile = models.ForeignKey(MyFile, blank=True, default=None, null=True)


# Automatically do something after user creation
def create_default_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = Profile.objects.get_or_create(owner=instance, alias='Myself', default=True)
post_save.connect(create_default_profile, sender=User)

@receiver(post_save, sender=Application)
def application_post_save(sender, **kwargs):
    application, created = kwargs["instance"], kwargs["created"]
    if created:
        assign_perm("view_application", application.owner, application)
        assign_perm("edit_application", application.owner, application)

@receiver(post_save, sender=Profile)
def profile_post_save(sender, **kwargs):
    profile, created = kwargs["instance"], kwargs["created"]
    if created:
        assign_perm("view_profile", profile.owner, profile)
        assign_perm("edit_profile", profile.owner, profile)
