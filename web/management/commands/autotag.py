# -*- coding: utf-8 -*-

import sys
import datetime
from django.core.management.base import BaseCommand, CommandError

from web.models import TemplateStructure, Template

class Command(BaseCommand):
    help = 'auto tag the form'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        self.stdout.write( 'start job at {0} \n'.format(datetime.datetime.now()), ending='')
        templates = Template.objects.filter(type='form')
        PRE_DEFINED_FIELD_ID = 57
        for template in templates:
            print template.myfile.img
            # Process file pngs and get the tag
            # item, created = TemplateStructure.get_or_create(template=template, fieldmeta=xxxxx)
            # item.field_id = PRE_DEFINED_FIELD_ID
            # item.save()
        self.stdout.write( 'completed job at {0} \n'.format(datetime.datetime.now()), ending='')