# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView
from django.core.files import File
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, HttpResponseRedirect
from models import Application, TemplateStructure, TagMap, \
    ProfileHistoricalData, Profile, Field, Template, Application, TEMPLATE_TYPE_CHOICES, MyFile, \
    ShareToken
from pyPdf import PdfFileWriter, PdfFileReader
import urllib, os
from subprocess import call
from django.utils import timezone

from guardian.decorators import permission_required
from reportlab.platypus import Image
from reportlab.pdfbase import pdfform
from reportlab.pdfgen import canvas  
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import letter
from wand.image import Image as WandImage

import json
import re
import base64
import hashlib
import time


import pdf
import utils

def collect_tags(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        width = int(data['pdf_width'])
        height = int(data['pdf_height'])
        template_id = data['template_id']
        has_predefined = False
        for tag in data['tags']:
            tag['left_top']['x'] = int(tag['left_top']['x']) 
            tag['left_top']['y'] = int(tag['left_top']['y']) 
            tag['right_bottom']['x'] = int(tag['right_bottom']['x']) 
            tag['right_bottom']['y'] = int(tag['right_bottom']['y']) 
            tag['pdf_width'] = width
            tag['pdf_height'] = height
            if 'tag_type' not in tag:
                tag['tag_type'] = 0
            if tag['tag_type'] != 0:
                has_predefined = True
                continue
            value = tag['value'].split(':')
            if len(value) > 2:
                return
            field = Field.objects.get(key=value[0])
            if len(value) == 2:
                options = json.loads(field.options)
                find = False
                for i in range(len(options)):
                    if options[i]['key'] == value[1]:
                        find = True
                        break
                if not find:
                    return
        for template_structure in TemplateStructure.objects.filter(template_id=template_id): 
            template_structure.delete()
        if has_predefined:
            predefined_field = Field.objects.get(key='predefined')
            predefined, created = TemplateStructure.objects.get_or_create(
                    template_id=template_id, field=predefined_field);
            predefined_meta = dict()
            predefined_num = 0
        for tag in data['tags']:
            if tag['tag_type'] > 0:
                #predefined
                predefined_num += 1
                predefined_meta[str(predefined_num)] = tag
            else:
                value = tag['value'].split(':')
                field = Field.objects.get(key=value[0])
                template_structure, created = TemplateStructure.objects.get_or_create(
                        template_id=template_id, field=field)
                key = "text"
                meta = dict()
                if len(value) == 2:
                    key = value[1]
                    if template_structure.field_meta != '':
                        meta = json.loads(template_structure.field_meta)
                meta[key] = tag
                template_structure.field_meta = json.dumps(meta);
                template_structure.save()
        if has_predefined:
            predefined.field_meta = json.dumps(predefined_meta)
            predefined.save()
        return HttpResponse(json.dumps({'success': True}), content_type='application/json')

def template_delete(request, template_id):
    record = Template.objects.get(id=template_id)
    record.delete()
    return HttpResponse(json.dumps({'success': True}), content_type='application/json')

def upload(request):
    if not request.POST:
        return HttpResponseBadRequest()
    upload_type = request.POST.get('upload_type', '')

    if upload_type == 'template':
        myfile = MyFile.objects.create(file=request.FILES["template_file"])
        myfile.save()
        myfile.create_thumbnail()
        template = Template.objects.create(
                name=request.POST["template_name"], 
                myfile=myfile, 
                type=TEMPLATE_TYPE_CHOICES[0][0])
        return redirect('template_detail', template_id=template.id)


def templates(request):
    templates = Template.objects.filter(type='form')
    return render(request, 'templates.html', {
        'user_extra_data': get_user_extra_data(request),
        'templates': templates
    });

def template_detail(request, template_id):
    template = Template.objects.get(id=template_id)
    tag_info = {'tags': []}
    for template_structure in TemplateStructure.objects.filter(template__id=template_id):
        if template_structure.field_meta != '':
            meta = json.loads(template_structure.field_meta)
            for m in meta:
                tag_info['tags'].append(meta[m])

    all_fields = []
    for field in Field.objects.all():
        if field.key == 'predefined':
            continue
        if field.options.strip() == '':
            all_fields.append((field.key, field.description))
        else:
            options = json.loads(field.options)
            for option in options:
                all_fields.append((field.key + ':' + option['key'], field.description + ':' + option['value']))
    return render(request, 'template_detail.html', {
        'name': template.name,
        'template_id': template_id,
        'pdf_url': template.myfile.file.url,
        'tag_string': base64.b64encode(json.dumps(tag_info)),
        'all_fields': base64.b64encode(json.dumps(all_fields)),
        'user_extra_data': get_user_extra_data(request),
    })


def get_user_extra_data(request):
    if request.user.socialaccount_set.all():
        social_account = request.user.socialaccount_set.all()[0]
        extra_data = social_account.extra_data
        if social_account.provider == 'google':
            extra_data['picture'] = extra_data['picture']+'?sz=100'
            extra_data['name'] = extra_data['name']
        elif social_account.provider == 'weixin':
            extra_data['name'] = extra_data['nickname']
            extra_data['picture'] = extra_data['headimgurl']
        return extra_data
    else:
        return {}

# Main console for all available forms
def applications(request):
    applications = Application.objects.filter(owner=request.user)
    return render(request, 'applications.html', {
        'applications': applications,
        'user_extra_data': get_user_extra_data(request)
    });

def application_choose_template(request):
    templates = Template.objects.filter(type='form')
    return render(request, 'search_country.html', {
        'templates': templates,
        'user_extra_data': get_user_extra_data(request)
    });

def application_create(request):
    template_id = request.POST.get('template')
    profile = Profile.objects.get(alias='Myself', owner=request.user)

    application = Application(owner=request.user, alias='', profile=profile, template_id=template_id)
    application.save()

    return redirect('application_detail', application_id=application.id)

def profiles(request):
    profile = Profile.objects.filter(owner=request.user)[0]
    return redirect('profile_detail', profile_id=profile.id)

@permission_required('view_profile', (Profile, 'id', 'profile_id'), return_403=True)
def profile_detail(request, profile_id):
    profile = get_object_or_404(Profile, id=profile_id)

    profile_data = profile_read_data(profile)
    fields = Field.objects.filter(show_in_profile=True).exclude(type='file').order_by('category')
    for field in fields:
        field.profile_data = profile_data.get('field_'+ str(field.id), '')

    return render(request, 'profile_detail.html', {
        'user_extra_data': get_user_extra_data(request),
        'fields': fields,
        'profiles': Profile.objects.filter(owner=request.user),
        'profile': profile,
    });

def profile_create(request):
    if not request.POST:
        return HttpResponseBadRequest()
    alias = request.POST.get('alias', '')
    profile, created = Profile.objects.get_or_create(
        owner=request.user,
        alias=alias)
    profile.save()
    return redirect('profile_detail', profile_id=profile.id)

def profile_save_data(request):
    if not request.POST:
        return HttpResponseBadRequest()

    profile_id = request.POST.get('profile_id', '')
    profile = get_object_or_404(Profile, id=profile_id)

    for k, v in request.POST.items():
        if k.startswith('field_'):
            # format: field_id:en
            field_id = k.split(':')[0].replace('field_', '')
            historical, created = ProfileHistoricalData.objects.get_or_create(
                field_id=field_id,
                profile_id=profile_id)
            if not v:
                historical.value = ''
            elif historical.field.type =='text':
                language = k.split(':')[1]
                historical.value = json.dumps(
                    utils.update_str_dict(historical.value, language, v), 
                    ensure_ascii=False,
                    encoding='utf8')    
            elif historical.field.type == 'digit':
                historical.value = v
            elif historical.field.type == 'single_choice':
                value_dict = utils.reset_str_dict(historical.value)
                value_dict[v] = 'checked'
                historical.value = json.dumps(value_dict, ensure_ascii=False) 
            elif historical.field.type == 'date':
                value_dict = utils.date_str_to_dict(v)
                historical.value = json.dumps(value_dict, ensure_ascii=False) 
            historical.save()
    return HttpResponse('Success')


def profile_read_data(profile):
    records = {}
    datas = ProfileHistoricalData.objects.filter(profile=profile)
    for data in datas:
        field_type = data.field.type
        value = ''
        if not data.value:
            value = ''
        elif field_type == 'text':
            value = utils.json_loads_byteified(data.value)
        elif field_type == 'digit':
            value = data.value
        elif field_type == 'single_choice':
            value = utils.rebuild_single_choice_field(data.value)
        elif field_type == 'date':
            value = utils.date_dict_to_str(data.value)
        records['field_' + str(data.field.id)] = value
    return records

def documents(request):
    records = ProfileHistoricalData.objects \
        .filter(profile__owner=request.user) \
        .exclude(myfile__isnull=True)
        
    return render(request, 'documents.html', {
        'user_extra_data': get_user_extra_data(request),
        'records': records,
    });


def format_application_records(application):
    if not application.output_data:
        return {}
    return dict(utils.json_loads_byteified(application.output_data))

def format_profile_records_for_template(profile, template):
    records = {}
    template_fields = TagMap.objects.filter(template=template, field__isnull=False)
    profile_fields = ProfileHistoricalData.objects.filter(profile=profile)
    for item in template_fields:
        try:
            data = profile_fields.get(field=item.field)
            if not data.value:
                records[item.tag] = ''
            elif item.field.type == 'digit':
                records[item.tag] = value
            else:
                value = dict(utils.json_loads_byteified(data.value))
                records[item.tag] = value.get(item.identifier, '')
        except ProfileHistoricalData.DoesNotExist:
            records[item.tag] = ''
            
    return records

def get_profile_and_application_records(application):
    profile_records = format_profile_records_for_template(application.profile, application.template)
    application_records =  format_application_records(application)
    profile_records.update(application_records)
    return profile_records

# Main Page for fill applications
@permission_required('view_application', (Application, 'id', 'application_id'), return_403=True)
def application_detail(request, application_id):
    application = get_object_or_404(Application, id=application_id)
    profiles = Profile.objects.filter(owner=request.user)
    profile_records = {}
    for profile in profiles:
        profile_records[profile.id] = format_profile_records_for_template(profile, application.template)

    return render(request, 'form_fill.html', {
        'pdf_html': application.template.myfile.html,
        'application': application,
        'records': json.dumps(get_profile_and_application_records(application)),
        'profile_records': json.dumps(profile_records),
        'language': application.template.language,
        'profiles': profiles,
        'user_extra_data': get_user_extra_data(request)
    })
  


@permission_required('view_application', (Application, 'id', 'application_id'), return_403=True)
def application_share_create(request, application_id):
    if request.method == 'POST':
        token = ''
        while True:
            m = hashlib.md5()
            m.update(application_id)
            m.update(str(time.time()))
            m.update('random salt')
            token = m.hexdigest()
            if not ShareToken.objects.filter(token=token).exists():
                item = ShareToken(token=token, owner=request.user, application_id=application_id)
                item.save()
                break
        return HttpResponse(token)

def application_share_view(request, token):
    share = get_object_or_404(ShareToken, token=token)

    diff_seconds = (timezone.now() - share.create_time).total_seconds()
    diff_hours = diff_seconds / 3600
    if diff_hours > 24:
        return HttpResponse('本次分享已过期')

    application = share.application
    return render(request, 'share.html', {
        'token': token,
        'pdf_html': application.template.myfile.html,
        'application': application,
        'records': json.dumps(get_profile_and_application_records(application)),
    })

def application_share_copy(request, token):
    share = get_object_or_404(ShareToken, token=token)

    diff_seconds = (timezone.now() - share.create_time).total_seconds()
    diff_hours = diff_seconds / 3600
    if diff_hours > 24:
        return HttpResponse('本次分享已过期')

    src_application = share.application

    # Use default profile and default application title
    profile = Profile.objects.get(alias='Myself', owner=request.user)
    application = Application(owner=request.user, alias='', profile=profile, template=src_application.template)

    # copy records that are not basic field.
    if src_application.output_data:
        src_data = dict(utils.json_loads_byteified(src_application.output_data))
        copied_data = {}
        for k, v in src_data.items():
            if not k.startswith('field_'):
                copied_data[k] = v
        application.output_data = json.dumps(copied_data)
    application.save()

    return redirect('application_detail', application_id=application.id)


def autosave(request):
    if not request.POST:
        return HttpResponseBadRequest()
    application_id = request.POST.get('application_id', '')
    application = get_object_or_404(Application, id=application_id)
    application.alias = request.POST.get('application_alias')

    output_data = request.POST.copy()
    del output_data['application_alias']
    del output_data['application_id']
    del output_data['csrfmiddlewaretoken']

    application.output_data = json.dumps(output_data, ensure_ascii=False)
    application.save()

    # Save latest field to historical
    for k, v in request.POST.items():
        try:
            tagmap = TagMap.objects.get(tag=k, template=application.template, field__isnull=False)
            field = tagmap.field
            historical, created = ProfileHistoricalData.objects.get_or_create(
                field=field,
                profile=application.profile)
            if field.type == 'digit':
                historical.value = v
            else:
                tmp_value = json.loads(historical.value) if historical.value else {}
                tmp_value[tagmap.identifier] = v
                historical.value = json.dumps(tmp_value, ensure_ascii=False)
            historical.save()
        except TagMap.DoesNotExist:
            pass
    return HttpResponse('Success')
 

def append_pdf(input,output):
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]

def preview_process(request, application_id):
    application = get_object_or_404(Application, id=application_id)
    fields = dict(utils.json_loads_byteified(application.output_data))

    try: 
        pdf.FillPdf(fields, application.template.myfile.pdf, settings.LOCAL_APPLICATION_STORAGE, str(application_id))
        return HttpResponse('done')
    except UnicodeDecodeError:
        return HttpResponseBadRequest('表格中包含非法字符，请重试')

def preview(request, application_id):
    # application = get_object_or_404(Application, id=application_id)
    # return redirect(application.output_file.url)
    output_file = os.path.join(settings.LOCAL_APPLICATION_STORAGE, str(application_id) + '.pdf')
    with open(output_file, 'r') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=some_file.pdf'
        return response
    pdf.closed

def index(request):
    if request.user.is_authenticated():
        return redirect('application_list')
    else:
        return render(request, 'account/login.html');