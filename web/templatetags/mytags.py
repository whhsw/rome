from django import template
import json

register = template.Library()

@register.filter(name='access')
def access(data, arg):
    if not data:
        return None
    return data.get(arg, None)

@register.filter(name='loadjson')
def loadjson(data):
    if data.strip():
        return json.loads(data.strip())
    else:
        return None