# from django.contrib import admin
# import models

# admin.site.register(models.Field)
# admin.site.register(models.MyFile)
# admin.site.register(models.Template)
# admin.site.register(models.TemplateStructure)
# admin.site.register(models.Profile)
# admin.site.register(models.Application)
# admin.site.register(models.ProfileHistoricalData)


from django.contrib import admin
from django.apps import apps

myapp = apps.get_app_config('web')
for model in myapp.get_models():
    admin.site.register(model)