# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import login
from django.shortcuts import render
from django.shortcuts import redirect
from allauth.account.models import EmailAddress

from re import compile

# from wechatpy.enterprise import WeChatClient
# from accounts.models import UserProfile
# client = WeChatClient(settings.CORP_ID, settings.SECRET)

EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]

def isWechatUserAgent(request):
    return request.META.get('HTTP_USER_AGENT', '').find("MicroMessenger") > 0


def account_verified(user):
    if user.is_authenticated:
        result = EmailAddress.objects.filter(email=user.email)
        if len(result):
            return result[0].verified
    return False

class LoginRequiredMiddleware:
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """


    def process_request(self, request):
        assert hasattr(request, 'user')

        path = request.path_info.lstrip('/')
        if any(m.match(path) for m in EXEMPT_URLS):
            return

        # Do nothing if alreayd logged in
        if request.user.is_authenticated():
            # if email verified or already in email verfication path
            if account_verified(request.user):
                return
            else:
                return redirect('account_email_verification_sent')

        # # Handler for wechat browser
        # if isWechatUserAgent(request):
        #     # if under wechat browser
        #     code = request.GET.get('code', '')
        #     # print 'code is', code
        #     if code:
        #         try:
        #             user_info = client.oauth.get_user_info(code)
        #             try:
        #                 up = UserProfile.objects.get(enterprise_wechat_id=user_info.get('UserId'))
        #                 up.user.backend = 'django.contrib.auth.backends.ModelBackend'
        #                 login(request, up.user)
        #             except UserProfile.DoesNotExist:
        #                 return render(request, 'info.html', {
        #                     'content': u'请等待管理员审核通过',
        #                 });
        #         except Exception as e:
        #             return HttpResponseForbidden()
        #     else:
        #         url = client.oauth.authorize_url(request.get_full_path())
        #         return HttpResponseRedirect(url)
        # # Handler for none wechat browser
        # else:

        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

