# -*- coding: utf-8 -*-
import subprocess
import os

from django.conf import settings

from fdfgen import forge_fdf
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdftypes import resolve1

def Pdf2Html(src, output_dir, output_file):
    data_dir = os.path.join(settings.BASE_DIR, 'static', 'pdf2htmlEX')
    command = "pdf2htmlEX --fit-width 1024 --optimize-text 1 --space-as-offset 1 --process-outline 0 --process-form 1 " + \
        "--dest-dir %s --data-dir %s %s %s" % (
        output_dir, data_dir, src, output_file)
    subprocess.call(command.split(' ')) 

def FillPdf(raw_fields, src, output_dir, output_file):
    fields = {}
    for k, v in raw_fields.items():
        if v == 'checked':
            checkbox = k.split(':')
            fields[checkbox[0]] = checkbox[1]
        else:
            fields[k] = v

    fdf_path = os.path.join(output_dir, output_file + ".fdf")
    output_path = os.path.join(output_dir, output_file + ".pdf")
    fdf = forge_fdf("",fields,[],[],[])
    fdf_file = open(fdf_path, "w")
    fdf_file.write(fdf)
    fdf_file.close()
    command = "pdftk %s fill_form %s output %s flatten" % (src, fdf_path, output_path)
    subprocess.call(command.split(' '))

def getFormFieldList(filename):
    result = []
    fp = open(filename, 'rb')
    parser = PDFParser(fp)
    doc = PDFDocument(parser)
    fields = resolve1(doc.catalog['AcroForm'])['Fields']
    for i in fields:
        field = resolve1(i)
        name, value = field.get('T'), field.get('V')
        # print '{0}: {1}'.format(name, value)
        result.append(name)
    return result